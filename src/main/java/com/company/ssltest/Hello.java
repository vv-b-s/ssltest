package com.company.ssltest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/hello")
public class Hello {

    @GET
    public Response sayHello() {
        return Response.ok("Hello! This application is working!").build();
    }
}
